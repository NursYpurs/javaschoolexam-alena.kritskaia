package com.tsystems.javaschool.tasks.calculator;


import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement)  {
        // TODO: Implement the logic here


            statement = toPolish(statement);
            statement = String.valueOf(calculate(statement));
            statement = format(statement);
        return statement;


    }

    private static String format (String statement){
        double doubleFormat = Double.parseDouble(String.valueOf(statement));
        if (doubleFormat% 1.0 ==0 ){
            int temp = (int)doubleFormat;
            statement = String.valueOf(temp);
        }
        else {
            doubleFormat = new BigDecimal(doubleFormat).setScale(3,BigDecimal.ROUND_HALF_UP).doubleValue();
            statement = String.valueOf(doubleFormat);
        }
        return statement;
    }


    private static Double calculate(String statement){
        double A = 0;
        double B = 0;
        String stringTmp;
        Deque<Double> stack = new ArrayDeque<Double>();
        StringTokenizer stringTokenizer = new StringTokenizer(statement);
        while (stringTokenizer.hasMoreTokens()) {
            try {
                stringTmp = stringTokenizer.nextToken().trim();
                if (1 == stringTmp.length() && isOperation(stringTmp.charAt(0))) {
                    if (stack.size() < 2) {
                        throw new Exception(" blf=blf=blf");
                    }
                    B = stack.pop();
                    A = stack.pop();
                    switch (stringTmp.charAt(0)) {
                        case '+':
                            A += B;
                            break;
                        case '-':
                            A -= B;
                            break;
                        case '/':
                            A /= B;
                            break;
                        case '*':
                            A *= B;
                            break;
                        case '%':
                            A %= B;
                            break;
                        case '^':
                            A = Math.pow(A, B);
                            break;
                        default:
                            throw new Exception("Not valide operation " + stringTmp);
                    }
                    stack.push(A);
                } else {
                    A = Double.parseDouble(stringTmp);
                    stack.push(A);
                }
            } catch (Exception e) {
                try {
                    throw new Exception("NOT VALIDE sumvol");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (stack.size() > 1) {
            try {
                throw new Exception("bla-bla");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return stack.pop();
    }


    private static String toPolish(String statement) {

        StringBuilder subStack = new StringBuilder("");
        StringBuilder subOut = new StringBuilder();
        char charIn, charTmp;

        for (int i = 0; i < statement.length(); i++) {
            charIn = statement.charAt(i);
            if (isOperation(charIn)) {
                while ((subStack.length() > 0)) {
                    charTmp = subStack.substring(subStack.length() - 1).charAt(0);
                    if (isOperation(charTmp) && isPrior(charIn) <= isPrior(charTmp)) {
                        subOut.append(" ").append(charTmp).append(" ");
                        subStack.setLength(subStack.length() - 1);
                    } else {
                        subOut.append(" ");
                        break;
                    }
                }
                subOut.append(" ");
                subStack.append(charIn);
            } else if ('(' == charIn) {
                subStack.append(charIn);
            } else if (')' == charIn) {
                charTmp = subStack.substring(subStack.length() - 1).charAt(0);
                while ('(' != charTmp) {
                    if (subStack.length() < 1) {
                        try {
                            throw new Exception("MISTAAAAAAAKE WITH bracket");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    subOut.append(" ").append(charTmp);
                    subStack.setLength(subStack.length() - 1);
                    charTmp = subStack.substring(subStack.length() - 1).charAt(0);
                }
                subStack.setLength(subStack.length() - 1);
            } else {
                subOut.append(charIn);
            }
        }

        while (subStack.length() > 0) {
            subOut.append(" ").append(subStack.substring(subStack.length() - 1));
            subStack.setLength((subStack.length() - 1));

        }
        return subOut.toString();
    }


    private static boolean isOperation(char c) {
        switch (c) {
            case '+':
            case '-':
            case '*':
            case '/':
                return true;
        }
        return false;
    }


    private static byte isPrior(char prior) {
        switch (prior) {
            case '*':
            case '/':
                return 2;
        }
        return 1;
    }
}